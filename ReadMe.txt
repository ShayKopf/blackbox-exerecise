###################### Debugging-Exercise ######################

This exercise's goal is to have the Hanichs 
debug a program both from a blackbox prespective
and a whitebox prespective.

During the exercise the Hanichs will be presented with
a pgroam called DebuggingTutorial. This program reads
files from a given directory and sends them to an IBM.MQ
queue. 
The program is written in Dotnet.Core and utilizes DI, Rx
and Nlog for logging.

In this repo you will find to folders:
One for a working version of the program
and a second one which we rigged.

the goal of this exercise is to fix the rigged program 
through debugging. We'll start by debugging the code
as a blackbox and slowly move to a whitebox debugging
and reading the code.

The Hanichs should guide the instructor and tell
him which action to perform. 
The instructors job is to guide them if they stray to
far, but also let them make mistake.


List of bugs\steps - Currently not "rigged"\implemented yet:

1. Invalid configuration file.

	Run the program. You will be presented with an 
	FormatException error:
	"Unhandled Exception: System.FormatException: Could not parse the JSON file. ---> 
	System.Text.Json.JsonReaderException: ':' is invalid after a single JSON value. 
	Expected end of data. LineNumber: 1 | BytePositionInLine: 22."
	
	By reading the stacktrace you can understand the program lies
	in the appsettings.json.
	by opening it you can notice it is missing a '{' in 
	the beginning of the file.
	
2. Bad Nlog.config file.
	
	Run the program yet again. This time you won't be presented with an error.
	Unfortunetly we can't really know if we succeded, we can check the queue
	and see that nothing was written to it.
	
	Here it gets a bit more complicated, we can decide to open the code,
	but by examining the compiltation folder we can see a NLog.config file
	and even a log.txt.
	Unfortunetly the log.txt file is empty although his date is new.
	
	Lets check it out and see why logs aren't being written.
	if we open the Nlog.config we can see a commented line:
	
	<!--<target name="logconsole" xsi:type="Console" />-->
	
	lets uncommented it.
	We can then see we only have rules to write to a file, and also only
	Fatal logs:
	<rules>
        <logger name="*" minlevel="Fatal" writeTo="logfile" />
    </rules>
	
	Lets change it to:	
	<rules>
        <logger name="*" minlevel="Debug" writeTo="logconsole" />
        <logger name="*" minlevel="Debug" writeTo="logfile" />
    </rules>
	
3. Input directory not found.
	
	Run the progam. Now we can see logs!
	You will again be presented with an 
	ArgumentException error, telling you:
	"The directory name 'C:\MqTeleporterPath' does not exist."
	
	This happens because we initialize a FileSystemWatcher with
	a path which does not exist.
	
	Create the path or modify the configuration for a valid path.

4. Mising configuration

	Running the program now results in the follwing logs:
	Trying to initialize IBM.MQWriter. QueueManager: , Queue:
	Stopped program because of exception: CompCode: 2, Reason: 2058
	
	Here we should start moving to whitebox debugging. The first log line
	is the most disturbing but we can also go google search the second line.
	
	Either way we should come to the conclusion that te error happens at 
	IbmMqWriter.cs
	
	
	Looking at where mqConfiguration is coming from we can see its a configuration
	which is supposed to come from the appsettings.json - lets open it and see whats the problem:
	
	{
	  "InputConfiguration": {
		"RootPath": "C:\\MqTeleporterPath"
	  }
	}
	- obviously there is no IBM.Mq config here.
	
Last step: IBM.MQ client version 9.1.4 is bugged.
	
	- Running the program results in a similar error we encountered before:
	  CompCode: 2, Reason: 2059.
	  
	  We can continue and debug this code but we'll never find the program.
	  We have to disassemble the IBM.MQ code to find the error.
	  
	  Searching on Google: "IBM.MQ client c# 9.1.4 exception" will result in 
	  stack overflow thread which provides us with the solution:
	  The IBM.MQ client of code 9.1.4 is bad!!
	  Upgrading to 9.1.5 will solve our issues.
	
	  Note: its important to specify the client version. Otherwise you won't
	  be able to find anything.
	
