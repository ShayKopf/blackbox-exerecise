﻿using DebuggingTutorial.Config;
using DebuggingTutorial.IBM;
using DebuggingTutorial.Input;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace DebuggingTutorial
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection serviceCollection)
        {

            // Build configuration
            var configuration = new ConfigurationBuilder()
               .SetBasePath(System.IO.Directory.GetCurrentDirectory()) //From NuGet Package Microsoft.Extensions.Configuration.Json
               .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
               .Build();

            // Load configuration:
            var inputSection = configuration.GetSection("InputConfiguration");
            serviceCollection.Configure<InputConfiguration>(options => inputSection.Bind(options));

            var mqConfigSection = configuration.GetSection("MqConfiguration");
            serviceCollection.Configure<IbmMqConfiguration>(options => inputSection.Bind(options));

            // Add access to generic IConfigurationRoot
            serviceCollection.AddSingleton(configuration);


            // Add app
            serviceCollection.AddTransient<InputSource, FileListener>()
               .AddTransient<IOutputWriter, IbmMqWriter>() 
               .AddLogging(loggingBuilder =>
               {
                   // configure Logging with NLog
                   loggingBuilder.ClearProviders();
                   loggingBuilder.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
                   loggingBuilder.AddNLog();
               })
               .BuildServiceProvider();
        }
    }
}
