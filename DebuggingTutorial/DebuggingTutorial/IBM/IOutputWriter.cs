﻿namespace DebuggingTutorial.IBM
{
    public interface IOutputWriter
    {
        void OutputData(byte[] data);
    }
}