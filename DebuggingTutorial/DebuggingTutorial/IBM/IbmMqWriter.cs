﻿using DebuggingTutorial.Config;
using IBM.WMQ;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace DebuggingTutorial.IBM
{
    public class IbmMqWriter : IOutputWriter
    {
        // The type of connection to use, this can be:-
        // MQC.TRANSPORT_MQSERIES_BINDINGS for a server connection.
        // MQC.TRANSPORT_MQSERIES_CLIENT for a non-XA client connection
        // MQC.TRANSPORT_MQSERIES_XACLIENT for an XA client connection
        // MQC.TRANSPORT_MQSERIES_MANAGED for a managed client connection
        const string connectionType = MQC.TRANSPORT_MQSERIES_MANAGED;

        private readonly MQQueue queue;
        private readonly IbmMqConfiguration mqConfiguration;

        public IbmMqWriter(ILogger<IbmMqWriter> logger, IOptions<IbmMqConfiguration> mqConfiguration)
        {
            this.mqConfiguration = mqConfiguration.Value;
            logger.LogDebug($"Trying to initialize IBM.MQWriter. QueueManager: {this.mqConfiguration.QueueManager}, Queue: {this.mqConfiguration.QueueName}");
            Hashtable connectionProperties = Init(connectionType);

            // Create a connection to the queue manager using the connection
            // properties just defined
            MQQueueManager qMgr = new MQQueueManager(this.mqConfiguration.QueueManager, connectionProperties);

            // Set up the options on the queue we want to open
            int openOptions = MQC.MQOO_INPUT_AS_Q_DEF | MQC.MQOO_OUTPUT;

            // Now specify the queue that we want to open,and the open options
            queue =
              qMgr.AccessQueue(this.mqConfiguration.QueueName, openOptions);
        }

        /// <summary>
        /// Initialise the connection properties for the connection type requested
        /// </summary>
        /// <param name="connectionType">One of the MQC.TRANSPORT_MQSERIES_ values</param>
        private Hashtable Init(string connectionType)
        {
            Hashtable connectionProperties = new Hashtable();

            // Add the connection type
            connectionProperties.Add(MQC.TRANSPORT_PROPERTY, connectionType);

            // Set up the rest of the connection properties, based on the
            // connection type requested
            switch (connectionType)
            {
                case MQC.TRANSPORT_MQSERIES_BINDINGS:
                    break;
                case MQC.TRANSPORT_MQSERIES_CLIENT:
                case MQC.TRANSPORT_MQSERIES_XACLIENT:
                case MQC.TRANSPORT_MQSERIES_MANAGED:
                    connectionProperties.Add(MQC.HOST_NAME_PROPERTY, mqConfiguration.HostName);
                    connectionProperties.Add(MQC.CHANNEL_PROPERTY, mqConfiguration.Channel);
                    break;
            }

            return connectionProperties;
        }

        public void OutputData(byte[] data)
        {
            try
            {
                // Define an IBM MQ message, writing some text in UTF format
                MQMessage message = new MQMessage();
                message.Write(data);

                // Specify the message options
                MQPutMessageOptions pmo = new MQPutMessageOptions(); // default message options

                // Put the message on the queue
                queue.Put(message, pmo);
            }

            //If an error has occurred,try to identify what went wrong.

            //Was it an IBM MQ error?
            catch (MQException ex)
            {
                Console.WriteLine("An IBM MQ error occurred: {0}", ex.ToString());
            }

            catch (Exception ex)
            {
                Console.WriteLine("A System error occurred: {0}", ex.ToString());
            }
        }
    }
}
