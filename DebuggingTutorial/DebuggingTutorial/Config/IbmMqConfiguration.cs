﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DebuggingTutorial.Config
{
    public class IbmMqConfiguration
    {
        public string QueueManager { get; set; }

        public string QueueName { get; set; }

        public string HostName { get; set; }

        public string Channel { get; set; }

        public string ConectionString { get; set; }
    }
}
