﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DebuggingTutorial.Input
{
    public class InputMessage
    {
        public string Identifier { get; set; }
        public byte[] Data { get; set; }
    }
}
