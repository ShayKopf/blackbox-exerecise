﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DebuggingTutorial.Input
{
    public interface InputSource
    {
        /// <summary>
        /// A subject which raises an even when a file has arrived
        /// </summary>
        IObservable<InputMessage> NewInput { get; }
    }
}
