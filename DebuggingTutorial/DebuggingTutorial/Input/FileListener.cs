﻿using DebuggingTutorial.Config;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;

namespace DebuggingTutorial.Input
{
    public class FileListener : InputSource
    {
        private readonly ILogger<FileListener> logger;
        private readonly FileSystemWatcher watcher;

        /// <summary>
        /// A subject which raises an even when a file has arrived
        /// </summary>
        public IObservable<InputMessage> NewInput { get; }

        public FileListener(ILogger<FileListener> logger, IOptions<InputConfiguration> inputConfiguration)
        {
            this.logger = logger;
            logger.LogDebug(inputConfiguration.Value.RootPath);

            watcher = new FileSystemWatcher(inputConfiguration.Value.RootPath);
            NewInput = Observable.FromEventPattern<FileSystemEventHandler, FileSystemEventArgs>(h => watcher.Created += h, h => watcher.Created -= h).Select(x => OnNewFile(x));
            watcher.EnableRaisingEvents = true;
        }

        protected InputMessage OnNewFile(EventPattern<FileSystemEventArgs> args)
        {
            logger.LogInformation($"Received new file: {args.EventArgs.FullPath}.");
            var message = new InputMessage
            {
                Identifier = args.EventArgs.FullPath,
                Data = File.ReadAllBytes(args.EventArgs.FullPath)
            };
            File.Delete(args.EventArgs.FullPath);
            return message;
        }
    }
}
