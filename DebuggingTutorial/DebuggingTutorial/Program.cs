﻿using DebuggingTutorial.IBM;
using DebuggingTutorial.Input;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NLog;
using System;
using System.IO;

namespace DebuggingTutorial
{
    class Program
    {
        static void Main(string[] args)
        {
            var logger = LogManager.GetCurrentClassLogger();
            var startupDI = new Startup();
            ServiceCollection serviceCollection = new ServiceCollection();
            startupDI.ConfigureServices(serviceCollection);

            var servicesProvider = serviceCollection.BuildServiceProvider();

            try
            {
                using (servicesProvider as IDisposable)
                {
                    var fileListener = servicesProvider.GetRequiredService<InputSource>();
                    var outputWriter = servicesProvider.GetRequiredService<IOutputWriter>();

                    fileListener.NewInput.Subscribe(inputMessage =>
                        {
                            logger.Info($"New input: {inputMessage.Identifier}");
                            outputWriter.OutputData(inputMessage.Data);
                        });

                    logger.Info("Press ANY key to exit");
                    Console.ReadKey();
                }
            }
            catch (Exception ex)
            {
                // NLog: catch any exception and log it.
                logger.Error(ex, $"Stopped program because of exception: {ex}");
            }
            finally
            {
                // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
                LogManager.Shutdown();
            }
        }
    }
 }
